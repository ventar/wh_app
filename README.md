/*
 * @Author: Ventar 
 * @Date: 2017-08-30 10:36:30 
 * @Last Modified by:   Ventar 
 * @Last Modified time: 2017-08-30 10:36:30 
 */

武汉竞赛测评，开发人员（温源），界面设计（余倩）。


```
.
|-- 0首页.html
|-- 1成绩单.html
|-- 2历史成绩.html
|-- 3排行榜.html
|-- 4综合测评.html
|-- 5单选题.html
|-- 6图片题.html
|-- 7音频题.html
|-- 8多选图片题.html
|-- 9结果页.html
|-- README.md
|-- css
|   |-- appraisal.css
|   |-- font.css
|   |-- result.css
|   |-- style.css
|   `-- style.css.map
|-- fonts
|   |-- icomoon.eot
|   |-- icomoon.svg
|   |-- icomoon.ttf
|   `-- icomoon.woff
|-- img
|   |-- bg_01.png
|   |-- bg_02.png
|   |-- custom_bottom_bg.jpg
|   |-- custom_but1.png
|   |-- custom_but2.png
|   |-- custom_but3.png
|   |-- custom_but4.png
|   |-- custom_but5.png
|   |-- custom_list_bg.png
|   |-- custom_pic_01.png
|   |-- custom_screen.png
|   |-- custom_search_bg.png
|   |-- custom_share.png
|   |-- erya_ico_00.png
|   |-- erya_ico_01.png
|   |-- erya_ico_02.png
|   |-- erya_ico_03.png
|   |-- erya_ico_04.png
|   |-- erya_ico_05.png
|   |-- erya_ico_06.png
|   |-- erya_ico_07.png
|   |-- erya_ico_08.png
|   |-- erya_ico_09.png
|   |-- erya_ico_10.png
|   |-- erya_ico_11.png
|   |-- erya_ico_12.png
|   |-- erya_img_01.png
|   |-- erya_img_02.png
|   |-- erya_img_03.png
|   |-- erya_img_04.png
|   |-- erya_img_05.png
|   |-- erya_img_06.png
|   |-- erya_img_07.png
|   |-- erya_img_08.png
|   |-- erya_img_09.png
|   |-- erya_img_10.png
|   |-- erya_img_11.png
|   |-- erya_img_12.png
|   |-- erya_img_13.png
|   |-- erya_img_14.png
|   |-- erya_img_15.png
|   |-- erya_img_16.png
|   |-- erya_img_17.png
|   |-- erya_img_18.png
|   |-- erya_img_18i.png
|   |-- erya_img_18i_1.png
|   |-- erya_img_18i_2.png
|   |-- erya_img_19.png
|   |-- erya_img_19i.png
|   |-- erya_img_19i_1.png
|   |-- erya_img_19i_2.png
|   |-- erya_img_21.png
|   |-- erya_img_22.png
|   |-- erya_img_23.png
|   |-- erya_img_24.png
|   |-- erya_index_01.png
|   |-- erya_index_02.png
|   |-- hd001_menu_01.png
|   |-- hd001_menu_02.png
|   |-- hd001_menu_03.png
|   |-- hd001_menu_04.png
|   |-- hd001_menu_05.png
|   |-- hd001_menu_06.png
|   |-- hd001_menu_07.png
|   |-- hd001_menu_08.png
|   |-- hd001_menu_bg.png
|   |-- hd001_title.png
|   |-- icon_excel.png
|   |-- icon_lv1.png
|   |-- icon_lv2.png
|   |-- icon_lv3.png
|   |-- icon_lv4.png
|   |-- icon_lv5.png
|   |-- icon_lv6.png
|   |-- icon_ppt.png
|   |-- icon_star.png
|   |-- icon_word.png
|   |-- img_01.png
|   |-- img_02.png
|   |-- img_03.png
|   |-- img_04.png
|   |-- img_05.png
|   |-- img_06.png
|   |-- img_07.png
|   |-- img_08.png
|   |-- menu_bg_1.png
|   |-- menu_bg_2.png
|   |-- no_image.png
|   |-- no_image_1.jpg
|   |-- o_pic.png
|   |-- on_1.png
|   |-- user_1.png
|   |-- user_2.png
|   `-- user_3.png
|-- js
|   |-- autoSize.js
|   |-- echarts.min.js
|   |-- jquery.min.js
|   `-- main.js
|-- sass
|   |-- base
|   |   `-- _reset.scss
|   |-- components
|   |   |-- _animation.scss
|   |   |-- _box-center.scss
|   |   |-- _buttons.scss
|   |   |-- _caret.scss
|   |   |-- _gotop.scss
|   |   |-- _loading.scss
|   |   |-- _popup.scss
|   |   |-- _position.scss
|   |   |-- _scott.scss
|   |   |-- _step.scss
|   |   `-- _table.scss
|   |-- layout
|   |   `-- _images.scss
|   |-- pages
|   |   |-- _home.scss
|   |   |-- _record.scss
|   |   `-- _test.scss
|   |-- style.scss
|   |-- utils
|   |   |-- _config.scss
|   |   `-- _mixins.scss
|   `-- vendors
|       `-- _calendar.scss
`-- upload
    |-- audio
    |   `-- audio.mp3
    `-- image
        |-- 8.jpg
        |-- custom_slider001.jpg
        |-- custom_slider002.jpg
        |-- slider001.jpg
        |-- test001.jpg
        |-- test002.jpg
        |-- test003.jpg
        |-- test004.jpg
        |-- test005.jpg
        `-- test006.jpg

14 directories, 149 files
```